package main

import (
	c "gitlab.com/otus-go-hw/homework-13/pkg/calendar"
	"time"
)

func main() {
	var calendar c.Calendar
	calendar = c.NewCalendar()

	// Добавление нового события.
	event := &c.Event{
		Id:          1,
		Title:       "Morning coffee",
		Description: "The most important event of the day",
		Date:        time.Now(),
	}
	_ = calendar.Add(event)
}
